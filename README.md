# README #

A simple service sample project.

## Language
* API ->  [Java:6](http://www.oracle.com/technetwork/java/javase/overview/index.html)
* Implementation -> [Java:8+](http://www.oracle.com/technetwork/java/javase/overview/index.html)
* Testing -> [Scala:2.12+](https://www.scala-lang.org/)

## Build tools
* [Gradle:3.4+](https://gradle.org/)
* [Nebula](http://nebula-plugins.github.io/)

## CDI
* [Spring:4+](http://projects.spring.io/spring-framework/)

## Service definition
* [Protocol Buffers:3+](https://developers.google.com/protocol-buffers/)
* [Grpc:1.2+](http://www.grpc.io/)

## Logging
* [SLF4J:1.7+](https://www.slf4j.org/)
* [logback:1.2+](https://logback.qos.ch/)

## Persistence
* [Spring Jdbc:4+](https://docs.spring.io/spring/docs/current/spring-framework-reference/html/jdbc.html)
* [JOOQ:4+](https://www.jooq.org/)
* [Hibernate:6+](http://hibernate.org/)

## Testing
* [Scalatest:3+](http://www.scalatest.org/)
* [Scalacheck:1+](https://www.scalacheck.org/)

##Dev libs
* [Lombok:+](https://projectlombok.org)
