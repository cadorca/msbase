package com.payulatam.fraudvault.lists.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by camilo.duran on 7/04/2017.
 */
@Entity
@Data
@EqualsAndHashCode(of = {"id"})
@Builder
public class AccessList {

    @Id
    private Long id;

    private String numero;

    private String emailComprador;

}
