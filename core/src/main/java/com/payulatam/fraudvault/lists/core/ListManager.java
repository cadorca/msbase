package com.payulatam.fraudvault.lists.core;

import com.payulatam.fraudvault.lists.domain.AccessList;
import com.payulatam.fraudvault.lists.persistence.ListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by camilo.duran on 7/04/2017.
 */
@Component
public class ListManager {

    @Autowired
    private ListRepository listRepository;

    public AccessList createList(AccessList accessList){
        return listRepository.save(accessList);
    }

    public void deleteList(long accessListId){
        listRepository.delete(accessListId);
    }

}
