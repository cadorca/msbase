package com.payulatam.fraudvault.lists.persistence;

import com.payulatam.fraudvault.lists.domain.AccessList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by camilo.duran on 7/04/2017.
 */
@Component
@Slf4j
public class ListRepository {

    private Set<AccessList> lists;
    private AtomicLong atomicLong;

    public ListRepository() {
        lists = new HashSet<>();
        atomicLong = new AtomicLong(0);
    }

    public AccessList save(AccessList accessList) {
        accessList.setId(atomicLong.incrementAndGet());
        lists.add(accessList);
        System.out.println("creating a list"+accessList);
        return accessList;
    }

    public void delete(long accessListId) {
        lists.remove(AccessList.builder().id(accessListId).build());
    }
}
