package com.payulatam.fraudvault.lists.service;

import com.payulatam.lists.grpc.AccessListManagerGrpc;
import com.payulatam.lists.grpc.ListCreation;
import com.payulatam.lists.grpc.ListCreationResult;
import io.grpc.*;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by camilo.duran on 6/04/2017.
 */
@Slf4j
public class AccessListClient {

    private final ManagedChannel channel;
    private final AccessListManagerGrpc.AccessListManagerBlockingStub accesListClient;
    private final AccessListManagerGrpc.AccessListManagerStub bListAsync;

    public AccessListClient() {
        channel = ManagedChannelBuilder
                .forTarget("localhost:50051")
                .usePlaintext(true).build();
        accesListClient = AccessListManagerGrpc.newBlockingStub(channel);
        bListAsync = AccessListManagerGrpc.newStub(channel);
    }

    public void createList(String email , String creditcard) {
        ListCreation listCreation = ListCreation.newBuilder().setCreditCardNumber(creditcard).setEmail(email).build();
        ListCreationResult listCreationResult = accesListClient.addList(listCreation);
        System.out.println(listCreation);
        System.out.println(listCreationResult);
    }

    public void close() throws InterruptedException {
        channel.shutdown();
        channel.awaitTermination(10, TimeUnit.SECONDS);
    }

    public static void main(String[] args) throws InterruptedException {
        AccessListClient client = new AccessListClient();
        Scanner scanner = new Scanner(System.in);
        String line;
        while(!(line = scanner.nextLine()).equals("exit")){
            String[] data = line.split(";");
            client.createList(data[0], data[1]);
        }
        client.close();
    }
}
