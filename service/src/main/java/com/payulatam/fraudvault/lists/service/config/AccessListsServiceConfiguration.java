package com.payulatam.fraudvault.lists.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by camilo.duran on 7/04/2017.
 */
@Configuration
@ComponentScan({"com.payulatam.fraudvault.lists.*"})
public class AccessListsServiceConfiguration {
}
