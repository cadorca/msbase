package com.payulatam.fraudvault.lists.service;

import com.payulatam.fraudvault.lists.service.config.AccessListsServiceConfiguration;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * Created by camilo.duran on 6/04/2017.
 */
@Slf4j
public class AccessServer {

    private Server server;
    private AnnotationConfigApplicationContext ctx;

    private void start() throws IOException {
        ctx = new AnnotationConfigApplicationContext(AccessListsServiceConfiguration.class);
        int port = 50051;
        server = ServerBuilder.forPort(port)
                .addService(ctx.getBean(AccessListService.class))
                .build()
                .start();
        log.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // Use stderr here since the logger may have been reset by its JVM shutdown hook.
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            AccessServer.this.stop();
            System.err.println("*** server shut down");
        }));
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    /**
     * Main launches the server from the command line.
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        final AccessServer server = new AccessServer();
        server.start();
        server.blockUntilShutdown();
    }
}
