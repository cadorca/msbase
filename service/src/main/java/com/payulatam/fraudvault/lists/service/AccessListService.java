package com.payulatam.fraudvault.lists.service;

import com.payulatam.fraudvault.lists.core.ListManager;
import com.payulatam.fraudvault.lists.domain.AccessList;
import com.payulatam.lists.grpc.*;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by camilo.duran on 6/04/2017.
 */
@Component
public class AccessListService extends AccessListManagerGrpc.AccessListManagerImplBase {

    private ArrayList<String> created = new ArrayList<>();

    @Autowired
    private ListManager listManager;

    @Override
    public void addList(ListCreation request, StreamObserver<ListCreationResult> responseObserver) {
        AccessList accessList = AccessList.builder()
                .emailComprador(request.getEmail())
                .numero(request.getCreditCardNumber())
                .build();
        AccessList createdList = listManager.createList(accessList);
        ListCreationResult result = ListCreationResult.newBuilder()
                .setId(createdList.getId())
                .setResult(ListProcessingResult.OK)
                .build();
        responseObserver.onNext(result);
        responseObserver.onCompleted();
    }

    @Override
    public void removeList(ListRemoval request, StreamObserver<ListDeleteResult> responseObserver) {
        listManager.deleteList(request.getId());
        ListDeleteResult result = ListDeleteResult.newBuilder().setResult(ListProcessingResult.OK).build();
        responseObserver.onNext(result);
        responseObserver.onCompleted();
    }

}
